/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static String[][] a = {{"-", "-", "-"},
    {"-", "-", "-"},
    {"-", "-", "-"}};
    static String currentplayer = "X";
    static int row = 0;
    static int col = 0;
    static String ans = null;
    static int count = 0;

    static void printHello() {
        System.out.println("Welcome XO");
    }

    static void printXplayfirst() {
        System.out.println("This Game X Always Start First");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    static void printTurn() {
        if (currentplayer == "X") {
            System.out.println(currentplayer + " " + "Turn");
        } else {
            System.out.println(currentplayer + " " + "Turn");
        }
    }

    static void switchPlayer() {
        if (currentplayer == "X") {
            currentplayer = "O";
        } else {
            currentplayer = "X";
        }
    }

    static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (a[row - 1][col - 1] != "-") {
                continue;
            }
            a[row - 1][col - 1] = currentplayer;
            break;
        }
    }

    static void printWinner() {
        System.out.println(currentplayer + " Winner!!!");
    }

    static void printDraw() {
        System.out.println("Draw!!!");
    }

    static boolean isWin() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkDiagonalRight()) {
            return true;
        } else if (checkDiagonalLeft()) {
            return true;
        }
        return false;
    }

    static void countRound() {
        count++;
    }

    static boolean isDraw() {
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    static boolean checkDraw() {
        if (count < 9) {
            return false;
        }
        count++;
        return true;
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (a[row - 1][i] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (a[i][col - 1] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDiagonalRight() {
        for (int i = 0; i < 3; i++) {
            if (a[i][i] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDiagonalLeft() {
        if(a[0][2].equals(currentplayer) && a[1][1].equals(currentplayer) && a[2][0].equals(currentplayer)){
            return true;
        }else{
            return false;
        }
    }

    static boolean continueGame() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Continue(Y/N)?");
        ans = kb.next();
        if("Y".equals(ans)) {
            return true;
        }else{
            return false;
        }
    }

    static void resetGame() {
        String[][] b = {{"-", "-", "-"},
        {"-", "-", "-"},
        {"-", "-", "-"}};
        a = b;
        count = 0;
        currentplayer = "X";
    }

    static void printEndgame(){
        System.out.println("Game End");
    }
    
    public static void main(String[] args) {
        printHello();
        printXplayfirst();
        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            countRound();
            if (isWin()) {
                printTable();
                printWinner();
                if(continueGame()) {
                    resetGame();
                    continue;
                }else{
                    printEndgame();
                    break;
                }
            } else if (isDraw()) {
                printTable();
                printDraw();
                if (continueGame()) {
                    resetGame();
                    continue;
                }else{
                    printEndgame();
                    break;
                }
            }
            switchPlayer();
        }
    }
}
